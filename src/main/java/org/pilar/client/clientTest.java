package org.pilar.client;

//import org.pilar.prueba.BuildingInfo;
//import org.pilar.prueba.BuildingInfoService;
//import org.pilar.prueba.IOException_Exception;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axis2.client.Options;
import org.apache.axis2.context.MessageContext;
import org.apache.axis2.context.ServiceContext;
import org.apache.axis2.databinding.ADBException;
import org.apache.axis2.description.MessageContextListener;
import com.foo.myservice.BuildingInfoServiceSkeleton;
import com.foo.myservice.BuildingInfoServiceMessageReceiverInOut;
import com.foo.myservice.BuildingInfoServiceStub;
import com.foo.myservice.IOExceptionException;
import org.apache.axis2.AxisFault;
import org.apache.axis2.client.Options;
import org.apache.axis2.description.MessageContextListener;
import org.apache.axis2.transport.http.HTTPConstants;
import org.apache.axis2.transport.http.impl.httpclient4.HttpTransportPropertiesImpl;
import org.pilar.prueba.*;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLStreamException;
import java.rmi.RemoteException;

public class clientTest {
    public static void main(String[] args) {
        final HttpTransportPropertiesImpl.Authenticator basicAuth = new HttpTransportPropertiesImpl.Authenticator();
        basicAuth.setUsername("admin");
        basicAuth.setPassword("admin");
        basicAuth.setPreemptiveAuthentication(true);


        BuildingInfoServiceStub stub = null;
        try {
            stub = new BuildingInfoServiceStub();
            final Options clientOptions = stub._getServiceClient().getOptions();
            clientOptions.setProperty(HTTPConstants.AUTHENTICATE, basicAuth);
            clientOptions.setTimeOutInMilliSeconds(300000);                     //I'm not sure what this line is for
            stub._getServiceClient().setOptions(clientOptions);
            stub._getServiceClient().getAxisService().addMessageContextListener(
                    new MessageContextListener() {
                        public void attachServiceContextEvent(final ServiceContext sc,
                                                              final MessageContext mc) {

                        }

                        public void attachEnvelopeEvent(final MessageContext mc) {

                            System.out.print("Got reply:");
                            try {
                                mc.getEnvelope().cloneOMElement().serialize(System.out);
                            } catch (XMLStreamException e) {
                                e.printStackTrace();
                            }
                            System.out.println();
                        }
                    }
            );
//            GetInfoE getInfoE = new GetInfoE();
//            getInfoE.setGetInfo(new GetInfo());
//            final GetInfoResponseE resp = a.getInfo(getInfoE);

            ValidPersonE validPersonE = new ValidPersonE();
            validPersonE.setValidPerson(new ValidPerson());

            ValidPerson person = new ValidPerson();
            Person p=new Person();
            p.setName("Pilar");
            p.setAge(88);
            final OMElement ele = p.getOMElement(QName.valueOf("person"), OMAbstractFactory.getOMFactory());
            System.out.println("Transmitting request:");
            System.out.println(ele.toStringWithConsume());
            person.setArg0(p);
            validPersonE.setValidPerson(person);
            final ValidPersonResponseE resp = stub.validPerson(validPersonE);

        } catch (AxisFault axisFault) {
            axisFault.printStackTrace();
        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (ADBException e) {
            e.printStackTrace();
        } catch (XMLStreamException e) {
            e.printStackTrace();
        }

    }
}